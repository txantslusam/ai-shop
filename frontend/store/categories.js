export const state = () => ({
  categoriesAll: [],
  categoriesSingle: {}
})

export const mutations = {
  setAll(state, data) {
    state.categoriesAll = data
  },

  setOne(state, data) {
    state.categoriesSingle = data
  }
}

export const actions = {
  async findAll({ commit }, payload) {
    const { data } = await this.$axios.get(`${process.env.api_url}/categories`, payload)
    commit('setAll', data)
  },

  async find({ commit }, id) {
    const { data } = await this.$axios.get(`${process.env.api_url}/categories/${id}`)
    commit('setOne', data)
  },

  async findOneBySlug({ commit, state }, slug) {
    const category = state.categoriesAll.find(category => category.slug === slug)
    if (category) {
      commit('setOne', category)
      return
    }

    const { data } = await this.$axios.get(`${process.env.api_url}/categories/slug/${slug}`)
    commit('setOne', data)
  },

  async create(context, payload) {
    await this.$axios.post(`${process.env.api_url}/categories`, payload)
  },

  async update(context, payload) {
    await this.$axios.put(`${process.env.api_url}/categories/${payload.id}`, payload)
  },

  async delete(context, id) {
    await this.$axios.delete(`${process.env.api_url}/categories/${id}`)
  }
}
