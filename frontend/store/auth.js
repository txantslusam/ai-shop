export const state = () => ({
  token: (localStorage && localStorage.getItem('token')) || '',
  user: (localStorage && localStorage.getItem('user') && JSON.parse(localStorage.getItem('user'))) || {}
})

export const mutations = {
  setUser(state, user) {
    state.user = user
    localStorage.setItem('user', JSON.stringify(user))
  },

  setToken(state, token) {
    state.token = token
    localStorage.setItem('token', token)
    this.$axios.setToken(token, 'Bearer')
  },

  purgeAuthData(state) {
    state.token = ''
    state.user = {}
    localStorage.removeItem('token')
    localStorage.removeItem('user')
  }
}

export const actions = {
  async login({ commit }, params) {
    const { data } = await this.$axios.post(`${process.env.api_url}/user/login`, params)
    const { user, token } = data

    commit('setUser', user)
    commit('setToken', token)
  },

  async register({ commit }, registerInfo) {
    const { data } = await this.$axios.post(`${process.env.api_url}/user`, registerInfo)
    commit('setUserData', data)
  },

  async passwordForgotRequest(context, params) {
    await this.$axios.post(`${process.env.api_url}/user/forgot-password`, params)
  },

  async passwordForgotRequestChange(context, params) {
    await this.$axios.post(`${process.env.api_url}/user/forgot-password/change`, params)
  }
}
