export const state = () => ({
  cart: (localStorage && localStorage.getItem('cart') && JSON.parse(localStorage.getItem('cart'))) || []
})

export const mutations = {
  addProduct(state, { product, count }) {
    // eslint-disable-next-line no-console
    console.log({ product, count })
    const { cart } = state
    const productInCart = cart.find(cartItem => cartItem._id === product._id)

    if (productInCart) {
      state.cart = cart.map((cartItem) => {
        if (cartItem._id === product._id) {
          cartItem.count += count
        }
        return cartItem
      })
      return
    }

    state.cart.push({
      ...product,
      count
    })
  },

  updateProduct(state, { product, count }) {
    state.cart = state.cart.map((cartItem) => {
      if (cartItem._id === product._id) {
        cartItem.count = count
      }
      return cartItem
    })
  },

  deleteProduct(state, product) {
    state.cart = state.cart.filter((cartItem) => {
      if (cartItem._id === product._id) {
        return false
      }
      return true
    })
  },

  updateLocalstorage(state) {
    localStorage.setItem('cart', JSON.stringify(state.cart))
  },

  reset(state) {
    state.cart = []
    localStorage.removeItem('cart')
  }
}

export const actions = {
  addProduct({ commit }, { product, count }) {
    commit('addProduct', { product, count })
    commit('updateLocalstorage')
  },

  updateProduct({ commit }, { product, count }) {
    commit('updateProduct', { product, count })
    commit('updateLocalstorage')
  },

  deleteProduct({ commit }, product) {
    commit('deleteProduct', product)
    commit('updateLocalstorage')
  }
}
