export const state = () => ({
  productsAll: [],
  productsSingle: {}
})

export const mutations = {
  setAll(state, data) {
    state.productsAll = data
  },

  setOne(state, data) {
    state.productsSingle = data
  }
}

export const actions = {
  async findAll({ commit }, payload) {
    const { data } = await this.$axios.get(`${process.env.api_url}/products/`, payload)
    commit('setAll', data)
  },

  async find({ commit }, id) {
    const { data } = await this.$axios.get(`${process.env.api_url}/products/${id}`)
    commit('setOne', data)
  },

  async findOneBySlug({ commit }, slug) {
    const { data } = await this.$axios.get(`${process.env.api_url}/products/single`, {
      params: {
        slug
      }
    })
    commit('setOne', data)
  },

  async findByQuery({ commit }, query) {
    const { data } = await this.$axios.get(`${process.env.api_url}/products/find`, {
      params: {
        query
      }
    })
    return data
  },

  async findAllInCategory({ commit }, category) {
    // eslint-disable-next-line no-console
    console.log(category)
    const { data } = await this.$axios.get(`${process.env.api_url}/products/category`, {
      params: {
        category
      }
    })
    commit('setAll', data)
  },

  async create(context, payload) {
    await this.$axios.post(`${process.env.api_url}/products/`, payload, {
      headers: { 'Content-Type': 'multipart/form-data' }
    })
  },

  async update(context, { id, payload }) {
    // eslint-disable-next-line no-console
    console.log(id, payload)
    await this.$axios.put(`${process.env.api_url}/products/${id}`, payload, {
      headers: { 'Content-Type': 'multipart/form-data' }
    })
  },

  async delete(context, id) {
    await this.$axios.delete(`${process.env.api_url}/products/${id}`)
  }
}
