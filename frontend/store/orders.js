export const state = () => ({
  ordersAll: [],
  ordersSingle: {}
})

export const mutations = {
  setAll(state, data) {
    state.ordersAll = data
  },

  setOne(state, data) {
    state.ordersSingle = data
  }
}

export const actions = {
  async findAll({ commit }, payload) {
    const { data } = await this.$axios.get(`${process.env.api_url}/orders/`, payload)
    commit('setAll', data)
  },

  async find({ commit }, id) {
    const { data } = await this.$axios.get(`${process.env.api_url}/orders/${id}`)
    commit('setOne', data)
  }
}
