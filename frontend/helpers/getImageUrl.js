const getImageUrl = (prefix, id) => `${process.env.api_url}/${prefix}/view/${id}`

export default getImageUrl
