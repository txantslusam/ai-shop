import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { HandlebarsAdapter, MailerModule } from '@nest-modules/mailer';

import { MONGO_URL } from './config';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { AuthService } from './auth/auth.service';
import { UserService } from './user/user.service';
import { ProductModule } from './product/product.module';
import { CategoryModule } from './category/category.module';
import {OrderModule} from "./orders/order.module";

@Module({
  imports: [
    MongooseModule.forRoot(MONGO_URL, {
      useNewUrlParser: true,
      useFindAndModify: false,
    }),
    MailerModule.forRoot({
      transport: {
        host: 'smtp.ethereal.email',
        port: 587,
        auth: {
            user: 'stella.jenkins@ethereal.email',
            pass: 'FN9XVeCMWrTJkvtyHX'
        }
      },
      defaults: {
        from:'"nest-modules" <modules@nestjs.com>',
      },
    }),
    AuthModule,
    UserModule,
    ProductModule,
    CategoryModule,
    OrderModule,
  ],
  controllers: [AppController],
  providers: [AppService, AuthService, UserService],
})
export class AppModule {}
