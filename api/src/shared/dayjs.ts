import * as customParseFormat from 'dayjs/plugin/customParseFormat';
const dayjs = require("dayjs");

dayjs.extend(customParseFormat);

export default dayjs;
