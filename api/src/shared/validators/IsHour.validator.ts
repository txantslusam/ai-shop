import { ValidationOptions, registerDecorator } from "class-validator";
import dayjs from "../dayjs";

export function IsHour(
  validationOptions?: ValidationOptions,
) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      name: 'IsHour',
      target: object.constructor,
      propertyName,
      options: validationOptions,
      validator: {
        validate(value: any) {
          const parsedValue = dayjs(value, 'HH:mm');
          return parsedValue.isValid();
        },
      },
    });
  };
}