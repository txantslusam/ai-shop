import { ValidationOptions, registerDecorator } from "class-validator";
import dayjs from "../dayjs";

export function IsDateShort(
  validationOptions?: ValidationOptions,
) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      name: 'IsDateShort',
      target: object.constructor,
      propertyName,
      options: validationOptions,
      validator: {
        validate(value: any) {
          const parsedValue = dayjs(value, 'YYYY-MM-DD');
          return parsedValue.isValid();
        },
      },
    });
  };
}