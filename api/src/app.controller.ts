import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { MailerService } from '@nest-modules/mailer';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly mailerService: MailerService,
  ) {}

  @Get()
  getHello(): string {
    this
    .mailerService
    .sendMail({
      to: 'test@nestjs.com', // sender address
      from: 'noreply@nestjs.com', // list of receivers
      subject: 'Testing Nest MailerModule ✔', // Subject line
      text: 'welcome', // plaintext body
      html: '<b>welcome</b>', // HTML body content
    })
    .then(() => {})
    .catch(() => {});
    
    return this.appService.getHello();
  }
}
