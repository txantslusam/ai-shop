import { Controller, UseGuards, Get, UsePipes, Post, Body, Put, Delete, Param } from '@nestjs/common';
import { CategoryService } from './category.service';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { CategoryData } from './category.interface';
import { ValidationPipe } from '../shared/pipes/validation.pipe';
import { CreateCategoryDTO } from './dto/create-category';
import { JwtPayload } from '../auth/interfaces/jwt-payload.interface';
import { UpdateCategoryDTO } from './dto/update-category';
import { User } from '../user/user.decorator';
import { RolesGuard } from '../auth/guards/roles.guard';

@Controller('categories')
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) {}

  @Get()
  async findAll(): Promise<CategoryData[]> {
    return await this.categoryService.findAll();
  }

  @Get('/slug/:slug')
  async findOneBySlug(@Param('slug') slug: string): Promise<CategoryData> {
    return await this.categoryService.findOneBySlug(slug);
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<CategoryData> {
    return await this.categoryService.findOne(id);
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @UsePipes(new ValidationPipe())
  @Post()
  async create(@Body() categoryData: CreateCategoryDTO) {
    return this.categoryService.create(categoryData);
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @UsePipes(new ValidationPipe())
  @Put(':id')
  async update(@Param('id') id: string, @Body() categoryData: UpdateCategoryDTO) {
    return this.categoryService.update(id, categoryData);
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Delete(':id')
  async delete(@Param('id') id: string) {
    return this.categoryService.delete(id);
  }
}
