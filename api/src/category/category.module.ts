import { Module, forwardRef } from '@nestjs/common';
import { CategoryController } from './category.controller';
import { CategorySchema } from './category.schema';
import { CategoryService } from './category.service';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Category', schema: CategorySchema }]),
  ],
  controllers: [
    CategoryController,
  ],
  providers: [
    CategoryService,
  ],
})
export class CategoryModule {}