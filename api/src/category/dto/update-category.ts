import { IsOptional } from 'class-validator';

export class UpdateCategoryDTO {
  @IsOptional()
  readonly name: string;
}