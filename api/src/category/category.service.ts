import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Mongoose } from 'mongoose';
import { CreateCategoryDTO } from './dto/create-category';
import { UpdateCategoryDTO } from './dto/update-category';
import { JwtPayload } from '../auth/interfaces/jwt-payload.interface';
import { CategoryModel } from './category.interface';
import dayjs from '../shared/dayjs';

@Injectable()
export class CategoryService {
  constructor(
    @InjectModel('Category') 
    private readonly categoryModel: Model<CategoryModel>,
  ) {}

  async findAll() {
    return await this.categoryModel
      .find()
      .exec();
  }
  
  async findOne(id: string) {
    const category = await this.categoryModel.findOne({
      _id: id,
    });

    if (!category) {
      throw new HttpException({ message: `category with id ${id} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    return category;
  }

  async findOneBySlug(slug: string) {
    const category = await this.categoryModel.findOne({
      slug,
    });

    if (!category) {
      throw new HttpException({ message: `category with slug ${slug} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    return category;
  }

  async create(dto: CreateCategoryDTO) {
    let slug = dto.name.toLowerCase().replace(/ /g, '-');
    let tempSlug = slug;
    let productWithSlug = await this.categoryModel.findOne({
      slug,
    })
      .exec()
    let nextId = 1;

    while (productWithSlug) {
      tempSlug = `${slug}-${nextId}`;
      productWithSlug = await this.categoryModel.findOne({
        slug: tempSlug,
      })
        .exec()
      nextId++;
    }

    slug = tempSlug;

    const newcategory = await this.categoryModel.create({
      ...dto,
      slug,
    });

    return newcategory;
  }

  async update(id: string, dto: UpdateCategoryDTO) {
    let slug = dto.name.toLowerCase().replace(/ /g, '-');
    let tempSlug = slug;
    let productWithSlug = await this.categoryModel.findOne({
      slug,
    })
        .exec()
    let nextId = 1;

    while (productWithSlug) {
      tempSlug = `${slug}-${nextId}`;
      productWithSlug = await this.categoryModel.findOne({
        slug: tempSlug,
      })
          .exec()
      nextId++;
    }

    slug = tempSlug;

    const toUpdate = await this.categoryModel.findOneAndUpdate({
        _id: id,
      }, {
        ...dto,
        slug,
      })
      .exec();

    if (!toUpdate) {
      throw new HttpException({ message: `Category with id ${id} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    return this.categoryModel.findById(id);
  }

  async delete(id: string) {
    const toDelete = await this.categoryModel.findOne({
      _id: id,
    });

    if (!toDelete) {
      throw new HttpException({ message: `Category with id ${id} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    await toDelete
      .remove();

    return 'OK';
  }
}
