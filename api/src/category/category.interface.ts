import { Document } from "mongoose";

export interface CategoryData {
  name: string,
  slug: string,
  
  created_at: Date,
  updated_at: Date,
}

export interface CategoryModel extends Document, CategoryData {}
