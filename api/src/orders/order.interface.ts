import { Document } from 'mongoose';
import {ProductInOrder} from './order.schema';

export interface OrderData {
  firstName: string;
  lastName: string;
  address: string;
  user: string;
  products: ProductInOrder;

  created_at: Date;
  updated_at: Date;
}

export interface OrderModel extends Document, OrderData {}
