import * as mongoose from 'mongoose';
import {ProductSchema} from "../product/product.schema";

export interface ProductInOrder {
  name: string;
  price: number;
  quantity: number;
}

export const OrderSchema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  address: String,
  products: [{
    name: String,
    price: Number,
    quantity: Number,
  }],
  user: mongoose.Types.ObjectId,
}, {
  timestamps: true,
});
