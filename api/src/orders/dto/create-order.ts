import { IsNotEmpty, IsOptional } from 'class-validator';
import { ProductInOrder } from '../order.schema';

export class CreateOrderDTO {
  @IsNotEmpty()
  readonly firstName: string;

  @IsNotEmpty()
  readonly lastName: string;

  @IsNotEmpty()
  readonly address: string;

  @IsNotEmpty()
  readonly products: ProductInOrder;
}
