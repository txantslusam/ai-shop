import { Controller, UseGuards, Get, UsePipes, Post, Body, Put, Delete, Param } from '@nestjs/common';
import { OrderService } from './order.service';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { OrderData } from './order.interface';
import { ValidationPipe } from '../shared/pipes/validation.pipe';
import { CreateOrderDTO } from './dto/create-order';
import { JwtPayload } from '../auth/interfaces/jwt-payload.interface';
import { User } from '../user/user.decorator';
import { RolesGuard } from '../auth/guards/roles.guard';

@Controller('orders')
export class OrderController {
  constructor(private readonly orderService: OrderService) {}

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get()
  async findAll(@User() user: JwtPayload): Promise<OrderData[]> {
    return await this.orderService.findAll();
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async findOne(@User() user: JwtPayload, @Param('id') id: string): Promise<OrderData> {
    return await this.orderService.findOne(id);
  }

  @UseGuards(JwtAuthGuard)
  @UsePipes(new ValidationPipe())
  @Post()
  async create(@User() user: JwtPayload, @Body() orderData: CreateOrderDTO) {
    return this.orderService.create(user, orderData);
  }
}
