import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateOrderDTO } from './dto/create-order';
import { JwtPayload } from '../auth/interfaces/jwt-payload.interface';
import { OrderModel } from './order.interface';

@Injectable()
export class OrderService {
  constructor(
    @InjectModel('Order') 
    private readonly orderModel: Model<OrderModel>,
  ) {}

  async findAll() {
    return await this.orderModel
        .find()
        .sort({
          date: 1,
          time: 1,
        })
        .exec();
  }

  async findAllByUser(user: JwtPayload) {
    return await this.orderModel
      .find({
        user: user.id,
      })
      .sort({
        date: 1,
        time: 1,
      })
      .exec();
  }

  async findOne(id: string) {
    const order = await this.orderModel.findOne({
      _id: id,
    });

    if (!order) {
      throw new HttpException({ message: `Order with id ${id} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    return order;
  }

  async findOneWithUserId(user: JwtPayload, id: string) {
    const order = await this.orderModel.findOne({
      _id: id,
      user: user.id,
    });

    if (!order) {
      throw new HttpException({ message: `Order with id ${id} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    return order;
  }

  async create(user: JwtPayload, dto: CreateOrderDTO) {
    const newOrder = await this.orderModel.create({
      ...dto,
      user: user.id,
    });

    return newOrder;
  }
}
