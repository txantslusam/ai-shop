import { Document } from 'mongoose';

export class User {
  email: string;
  role: string;
}

export interface UserModel extends Document {
  readonly email: string;
  readonly password: string,
  readonly isActive: boolean,
  readonly activationToken: string,
  readonly forgotPasswordToken: string,
  readonly role: string,
}

export class UserRO {
  user: User;
}

export class UserROLogin {
  user: User;
  token: string;
}

export interface ActivateAccountQuery {
  token: string,
  email: string,
}
