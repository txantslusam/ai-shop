import { Module, forwardRef } from '@nestjs/common';
import * as crypto from 'crypto';
import { UserController } from './user.controller';
import { UserSchema } from './user.schema';
import { UserService } from './user.service';
import { AuthService } from '../auth/auth.service';
import { AuthModule } from '../auth/auth.module';
import { MongooseModule, InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UserModel } from './user.interface';
import { EXAMPLE_USER } from '../config';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
    forwardRef(() => AuthModule),
  ],
  controllers: [
    UserController,
  ],
  providers: [
    UserService,
    AuthService,
  ],
  exports: [
    UserService,
  ],
})
export class UserModule {
  constructor(
    @InjectModel('User') 
    private readonly userModel: Model<UserModel>,
    private readonly userService: UserService,
  ) {
    this.userService.findOne(EXAMPLE_USER).then(user => {
      if (!user)
        this.userModel.create({
          ...EXAMPLE_USER,
          password: crypto.createHmac('sha256', EXAMPLE_USER.password).digest('hex')
        })
    });
  }
}