import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
  email: String,
  password: String,
  isActive: Boolean,
  activationToken: String,
  forgotPasswordToken: String,
  role: {
    type: String,
    enum: ['admin', 'customer'],
  }
});
