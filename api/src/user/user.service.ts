import { Model } from 'mongoose';
import * as crypto from 'crypto';

import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserRO, UserModel, ActivateAccountQuery } from './user.interface';
import { LoginUserDto, CreateUserDto, ForgotPasswordDto } from './dto';
import { MailerService } from '@nest-modules/mailer';
import { APP_URL, FRONTEND_APP_URL } from '../config';
import { ForgotPasswordChangeDto } from './dto/forgot-password.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectModel('User') 
    private readonly userModel: Model<UserModel>,
    private readonly mailerService: MailerService,
  ) {}

  async create(dto: CreateUserDto): Promise<UserRO> {

    // check uniqueness of username/email
    const { email, password } = dto;

    const user = await this.userModel.findOne({
      email,
    });

    if (user) {
      throw new HttpException({errors: { message: 'User with typed email already exists'}}, HttpStatus.BAD_REQUEST);
    }
    const activationToken = crypto.createHmac('sha256', email).digest('hex');

    // create new user
    const newUser = new this.userModel({
      email,
      activationToken,
      password: crypto.createHmac('sha256', password).digest('hex'),
      role: 'customer',
    });

    await newUser.save();

    await this.mailerService.sendMail({
      from: 'shop@ai.com',
      to: email,
      subject: 'Activate your account',
      html: `
        <body>
          <h1>Welcome to shop ai</h1>
          <p>To use your account you need to activate it.</p>
          <a href="${APP_URL}/user/activate?email=${email}&token=${activationToken}">Activate your account</a>
        </body>
      `,
    })
    return this.buildUserRO(newUser);
  }

  async forgotPassword({ email }: ForgotPasswordDto) {
    const user = await this.userModel.findOne({
      email,
    });

    if (user) {
      const token = crypto.createHmac('sha256', email).digest('hex');
      await user.update({
        forgotPasswordToken: token,
      })
        .exec()

      await this.mailerService.sendMail({
        from: 'shop@ai.com',
        to: email,
        subject: 'Password forgot email',
        html: `
          <body>
            <h1>Hello shop ai here.</h1>
            <p>To change password please follow the intructions in link.</p>
            <a href="${FRONTEND_APP_URL}/auth/forgot-password/change?email=${email}&token=${token}">Change your password</a>
          </body>
        `,
      })
    }

    return 'Forgot password email was sent successfully';
  }

  async forgotPasswordChange({ email, password, token }: ForgotPasswordChangeDto) {
    const user = await this.userModel.findOne({
      email,
      forgotPasswordToken: token,
    });
    
    if (!user) {
      throw new HttpException({errors: { message: 'User does not exists or you have wrong token'}}, HttpStatus.BAD_REQUEST);
    }

    if (user) {
      await user.update({
        password: crypto.createHmac('sha256', password).digest('hex'),
        forgotPasswordToken: '',
      })
        .exec()

      await this.mailerService.sendMail({
        from: 'shop@ai.com',
        to: email,
        subject: 'Password change success',
        html: `
          <body>
            <h1>Hello shop ai here.</h1>
            <p>You have successfully changed password.</p>
            <a href="${FRONTEND_APP_URL}/auth/login" target="_blank">Login now</a>
          </body>
        `,
      })
    }

    return 'Password changed successfully';
  }
  
  async findOne(loginUserDto: LoginUserDto): Promise<UserModel> {
    const findOneOptions = {
      email: loginUserDto.email,
      password: crypto.createHmac('sha256', loginUserDto.password).digest('hex'),
    };

    return await this.userModel.findOne(findOneOptions);
  }
  
  async findById({ id }): Promise<UserRO>{
    const user = await this.userModel.findById(id);

    if (!user) {
      return;
    }

    return this.buildUserRO(user);
  }

  async activateAccount({ token, email }: ActivateAccountQuery): Promise<string> {
    const user = await this.userModel.findOne({
      email,
    }).exec();

    if (!user) {
      throw new HttpException('User does not exists', HttpStatus.BAD_REQUEST);
    }

    if (user.isActive) {
      throw new HttpException('User is already active', HttpStatus.BAD_REQUEST);
    }

    if (token !== user.activationToken) {
      throw new HttpException('Activation token is unrecognized', HttpStatus.BAD_REQUEST);
    }

    await user.update({
      isActive: true,
    }).exec();

    return 'User activated successfully';
  }

  private buildUserRO(user: UserModel) {
    const userRO = {
      email: user.email,
      role: user.role,
    };

    return {user: userRO};
  }
}