import { createParamDecorator } from '@nestjs/common';
import { SECRET_KEY } from '../config';
import * as jwt from 'jsonwebtoken';

export const User = createParamDecorator((data, req) => {
  const token = req.headers.authorization ? (req.headers.authorization as string).split(' ') : null;
  if (token && token[1]) {
    const decoded: any = jwt.verify(token[1], SECRET_KEY);
    return decoded;
  }
});