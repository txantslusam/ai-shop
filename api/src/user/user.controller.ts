import { Post, Body, Controller, UsePipes, HttpException, HttpStatus, Inject, forwardRef, UseGuards, Get, Query } from '@nestjs/common';
import { UserService } from './user.service';
import { UserROLogin, UserRO, ActivateAccountQuery } from './user.interface';
import { LoginUserDto, CreateUserDto, ForgotPasswordDto } from './dto';
import { ValidationPipe } from '../shared/pipes/validation.pipe';
import { AuthService } from '../auth/auth.service';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { User } from './user.decorator';
import { JwtPayload } from '../auth/interfaces/jwt-payload.interface';
import { MailerService } from '@nest-modules/mailer';
import { ForgotPasswordChangeDto } from './dto/forgot-password.dto';

@Controller('user')
export class UserController {
  constructor(
    private readonly userService: UserService,
    @Inject(forwardRef(() => AuthService))
    private readonly authService: AuthService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  async findMe(@User() user: JwtPayload): Promise<UserRO> {
    return await this.userService.findById(user);
  }

  @UsePipes(new ValidationPipe())
  @Post()
  async create(@Body() userData: CreateUserDto) {
    return this.userService.create(userData);
  }

  @Post('forgot-password')
  async forgotPassword(@Body() data: ForgotPasswordDto) {
    return this.userService.forgotPassword(data);
  }

  @Post('forgot-password/change')
  async forgotPasswordChange(@Body() data: ForgotPasswordChangeDto) {
    return this.userService.forgotPasswordChange(data);
  }

  @UsePipes(new ValidationPipe())
  @Get('activate')
  async activate(@Query() activateAccountQuery: ActivateAccountQuery) {
    return this.userService.activateAccount(activateAccountQuery);
  }

  @UsePipes(new ValidationPipe())
  @Post('login')
  async login(@Body() loginUserDto: LoginUserDto): Promise<UserROLogin> {
    const _user = await this.userService.findOne(loginUserDto);

    if (!_user) throw new HttpException({errors: { message: 'Wrong email and/or password'}}, HttpStatus.BAD_REQUEST);
    
    const { id, email, isActive, role } = _user;

    if (!isActive) throw new HttpException({errors: { message: 'User is not activated'}}, HttpStatus.BAD_REQUEST);

    const token = await this.authService.createToken({
      id,
    });

    const { accessToken } = token;
    const user = { email, role };
    return { user, token: accessToken };
  }
}