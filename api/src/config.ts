export const MONGO_URL = 'mongodb://localhost:27017/ai-project';
export const SECRET_KEY = 'VerySecretKey';
export const EXAMPLE_USER = {
  email: 'admin@example.com',
  password: 'test123',
  isActive: true,
  role: 'admin',
};
export const APP_URL = 'http://localhost:3000';
export const FRONTEND_APP_URL = 'http://localhost:8080';
