import * as mongoose from 'mongoose';

export const ProductSchema = new mongoose.Schema({
  name: String,
  slug: String,
  price: Number,
  description: String,
  category: { type: mongoose.Schema.Types.ObjectId, ref: 'Category' },
  image: String,
}, {
  timestamps: true,
});

ProductSchema.index({'$**': 'text'})