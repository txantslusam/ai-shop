import { IsNotEmpty, IsOptional, IsNumber, IsNumberString, IsMongoId } from 'class-validator';
import { IsHour } from '../../shared/validators/IsHour.validator';
import { IsDateShort } from '../../shared/validators/IsDateShort.validator';

export class CreateProductDTO {
  @IsNotEmpty()
  readonly name: string;

  @IsNotEmpty()
  @IsNumberString()
  readonly price: Number;

  @IsNotEmpty()
  readonly description: string;

  @IsNotEmpty()
  @IsMongoId()
  readonly categoryId: string;
}
