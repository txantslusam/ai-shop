import { IsNotEmpty, IsOptional, IsNumberString, IsMongoId } from 'class-validator';
import { IsHour } from '../../shared/validators/IsHour.validator';
import { IsDateShort } from '../../shared/validators/IsDateShort.validator';

export class UpdateProductDTO {
  @IsOptional()
  readonly name: string;

  @IsOptional()
  @IsNumberString()
  readonly price: Number;

  @IsOptional()
  readonly description: string;

  @IsOptional()
  @IsMongoId()
  readonly categoryId: string;
}