import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Mongoose } from 'mongoose';
import { CreateProductDTO } from './dto/create-product';
import { UpdateProductDTO } from './dto/update-product';
import { JwtPayload } from '../auth/interfaces/jwt-payload.interface';
import { ProductModel } from './product.interface';
import dayjs from '../shared/dayjs';

@Injectable()
export class ProductService {
  constructor(
    @InjectModel('Product') 
    private readonly productModel: Model<ProductModel>,
  ) {}

  async findAll() {
    return await this.productModel
      .find()
      .populate('category')
      .exec();
  }
  
  async findOne(id: string) {
    const product = await this.productModel.findOne({
      _id: id,
    })
      .populate('category')
      .exec();

    if (!product) {
      throw new HttpException({ message: `Product with id ${id} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    return product;
  }

  async findOneBySlug(slug: string) {
    const product = await this.productModel.findOne({
      slug,
    })
      .populate('category')
      .exec();

    if (!product) {
      throw new HttpException({ message: `Product with slug ${slug} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    return product;
  }

  async findByQuery(query: string) {
    return await this.productModel
      .find({
        $or: [
          {
            name: { 
              $regex: query, 
              $options: "i" 
            },
          },
          {
            description: { 
              $regex: query, 
              $options: "i" 
            },
          }
        ]
      })
      .populate('category')
      .exec();
  }

  async findAllInCategory(category) {
    const products = await this.productModel
      .find({
        category,
      })
      .populate('category')
      .exec();

    return products;
  }

  async create(dto: CreateProductDTO, image: any) {
    let slug = dto.name.toLowerCase().replace(/ /g, '-');
    let tempSlug = slug;
    let productWithSlug = await this.productModel.findOne({
      slug,
    })
      .exec()
    let nextId = 1;

    while (productWithSlug) {
      tempSlug = `${slug}-${nextId}`;
      productWithSlug = await this.productModel.findOne({
        slug: tempSlug,
      })
        .exec()
      nextId++;
    }

    slug = tempSlug;
    
    const newProduct = this.productModel.create({
      ...dto,
      slug,
      category: dto.categoryId,
      image: image.filename,
    });

    return newProduct;
  }

  async update(id: string, dto: UpdateProductDTO, image: any) {
    const toUpdate = await this.productModel.findOne({
      _id: id,
    });
    
    if (!toUpdate) {
      throw new HttpException({ message: `Product with id ${id} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    let slug = dto.name.toLowerCase().replace(/ /g, '-');
    let tempSlug = slug;
    let productWithSlug = await this.productModel.findOne({
      slug,
    })
      .exec()
    let nextId = 1;

    if (toUpdate.slug !== slug) {
      while (productWithSlug) {
        tempSlug = `${slug}-${nextId}`;
        productWithSlug = await this.productModel.findOne({
          slug: tempSlug,
        })
          .exec()
        nextId++;
      }
    }

    slug = tempSlug;

    if (image) {
      await toUpdate.update({
        ...dto,
        slug,
        category: dto.categoryId,
        image: image.filename,
      })
        .exec();
    } else {
      await toUpdate.update({
        ...dto,
        slug,
        category: dto.categoryId,
        image: toUpdate.image,
      })
        .exec();
    }
    

    return this.productModel.findById(id);
  }

  async delete(id: string) {
    const toDelete = await this.productModel.findOne({
      _id: id,
    });

    if (!toDelete) {
      throw new HttpException({ message: `Place with id ${id} does not exist` }, HttpStatus.BAD_REQUEST);
    }

    await toDelete
      .remove();

    return 'OK';
  }
}
