import { Document } from "mongoose";
import { CategoryData } from "../category/category.interface";

export interface ProductData {
  name: string,
  slug: string,
  price: number,
  description: string,
  category: CategoryData,
  image: string,

  created_at: Date,
  updated_at: Date,
}

export interface ProductModel extends Document, ProductData {}
