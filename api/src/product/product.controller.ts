import { Controller, UseGuards, Get, UsePipes, Post, Body, Put, Delete, Param, UseInterceptors, UploadedFile, Res, HttpStatus, HttpException, Query } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from  'multer';
import { extname } from 'path';
import * as uuid from 'uuid/v4';
import { ProductService } from './product.service';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { ProductData } from './product.interface';
import { ValidationPipe } from '../shared/pipes/validation.pipe';
import { CreateProductDTO } from './dto/create-product';
import { JwtPayload } from '../auth/interfaces/jwt-payload.interface';
import { UpdateProductDTO } from './dto/update-product';
import { User } from '../user/user.decorator';
import { RolesGuard } from '../auth/guards/roles.guard';

@Controller('products')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Get()
  async findAll(): Promise<ProductData[]> {
    return await this.productService.findAll();
  }

  @Get('single')
  async findOneBySlug(@Query('slug') slug: string): Promise<ProductData> {
    return await this.productService.findOneBySlug(slug);
  }

  @Get('find')
  async findByQuery(@Query('query') query: string): Promise<ProductData[]> {
    return await this.productService.findByQuery(query);
  }

  @Get('category')
  async findAllInCategory(@Query('category') category: string) {
    return await this.productService.findAllInCategory(category);
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<ProductData> {
    return await this.productService.findOne(id);
  }

  @Get('view/:id')
  async findOneAndViewImage(@Param('id') id: string, @Res() res): Promise<void> {
    const product = await this.productService.findOne(id);

    if (!product.image) {
      return;
    }
    res.sendFile(product.image, { root: './storage/app/public/images'});
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @UsePipes(new ValidationPipe())
  @UseInterceptors(FileInterceptor('image', {
    storage: diskStorage({
      destination: './storage/app/public/images',
      filename: (req, file, cb) => cb(null, `${uuid()}${extname(file.originalname)}`),
    })
  }))
  @Post()
  async create(@Body() productData: CreateProductDTO, @UploadedFile() image) {
    if (!image) {
      throw new HttpException({message: 'Input data validation failed', errors:  { image: 'Image is empty' }}, HttpStatus.BAD_REQUEST);
    }

    return this.productService.create(productData, image);
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @UsePipes(new ValidationPipe())
  @UseInterceptors(FileInterceptor('image', {
    storage: diskStorage({
      destination: './storage/app/public/images',
      filename: (req, file, cb) => cb(null, `${uuid()}${extname(file.originalname)}`),
    })
  }))
  @Put(':id')
  async update(@Param('id') id: string, @Body() productData: UpdateProductDTO, @UploadedFile() image) {
    return this.productService.update(id, productData, image);
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Delete(':id')
  async delete(@Param('id') id: string) {
    return this.productService.delete(id);
  }
}
