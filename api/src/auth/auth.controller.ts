import { Controller, Get, UseGuards, Post, Body, UsePipes } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { ValidationPipe } from '../shared/pipes/validation.pipe';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @UsePipes(new ValidationPipe())
  @Post('token')
  async createToken(@Body() user: JwtPayload): Promise<any> {
    return await this.authService.createToken(user);
  }
}