import { Injectable, forwardRef, Inject } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { UserService } from '../user/user.service';
import { UserRO } from '../user/user.interface';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    @Inject(forwardRef(() => UserService))
    private readonly userService: UserService,
  ) {}

  async createToken(user: JwtPayload) {
    const accessToken = this.jwtService.sign(user);
    return {
      expiresIn: 3600 * 24 * 7,
      accessToken,
    };
  }

  async validateUser(payload: JwtPayload): Promise<UserRO> {
    // put some validation logic here
    // for example query user by id/email/username
    const user = await this.userService.findById(payload);
    return user;
  }
}