import { IsEmail, IsNotEmpty } from 'class-validator';

export class JwtPayload {
  @IsNotEmpty()
  id: string;
}